#!/bin/bash
# Based on https://community.webfaction.com/questions/10613/grant-permissions-to-extrasecondary-users/10658
# Modified to work outside the webapps directory too
# Modified to only grant read permissions

if [ $# -lt 2 -o $# -gt 3 ]; then
    echo
    echo 'Please provide 2 or 3 arguments.'
    echo
    echo 'USAGE: permissions.sh action username directory'
    echo 'actions: grant, revoke-app, revoke-all'
    echo 'eg: permissions.sh grant joe $HOME/webapps/django'
    echo
    # echo 'Currently only works for directories in /home/username/webapps.'
    # echo
    exit
fi

PRIMARY_USER=`whoami`
ACTION=$1
SECONDARY_USER=$2
APP_DIR=$3
DIR_ARRAY=$3

# Parse the supplied directory path
i=0
while [ "$DIR_ARRAY" != "/" ];do
    i=$(($i+1))
    parse[$i]="$(basename "$DIR_ARRAY")"
    DIR_ARRAY="$(dirname "$DIR_ARRAY")"
    if [ "$DIR_ARRAY" != "/home" -a "$DIR_ARRAY" != "/" ]; then
        [ "$DIR_ARRAY" = "." ] && DIR_ARRAY="$(pwd -P)"
    fi
done

if [ $ACTION == "grant" -a $# == 3 ]; then
    # Uncomment to disable using outside of ~/webapps
    # if [[ $APP_DIR != $HOME/webapps/* ]]; then
    #     echo
    #     echo "This script currently only works with $HOME/webapps"
    #     echo
    #     exit
    # fi

    echo 'Granting permissions to' $APP_DIR 'for user' $SECONDARY_USER'.'
    if [ ! `getfacl -p $HOME | grep user:$SECONDARY_USER:--x` ]; then
        # Grant secondary user access to the primary account's home directory
        # for navigational purposes only.
        setfacl -m u:$SECONDARY_USER:--x $HOME
        # Disallow ALL access to all direcories for secondary user.
        setfacl -R -m u:$SECONDARY_USER:--- $HOME/webapps/*
    fi

    DIR_PATH=""
    while [ $i -gt 0 ]; do
        DIR_PATH=$DIR_PATH/"${parse[$i]}"
        if [ "$DIR_PATH" != "/home" -a "$DIR_PATH" != "/" ]; then
            if [ "$DIR_PATH" != "$HOME" -a "$DIR_PATH" != "$HOME/webapps" ]; then
                if [ $i == 1 ]; then
                    # Grant secondary user permissions to specified directory.
                    setfacl -R -m u:$SECONDARY_USER:r-x $DIR_PATH
                    # Grant secondary user permissions to new directories created
                    # in specified directory.
                    setfacl -R -m d:u:$SECONDARY_USER:r-x $DIR_PATH
                    # Ensure all new directories and files are owned by the primary
                    # account's group.
                    chmod g+s $DIR_PATH
                    # Make sure the primary account user continues to have full access
                    # to all files and directories.
                    setfacl -R -m d:u:$PRIMARY_USER:rwx $DIR_PATH
                else
                    # Grant secondary user permissions to navigate to specified directory.
                    setfacl -R -m u:$SECONDARY_USER:--x $DIR_PATH
                fi
            fi
        fi
        i=$(($i-1))
    done

elif [ $ACTION == "revoke-app" ]; then
    echo 'Revoking permissions for user' $SECONDARY_USER 'to' $APP_DIR
    setfacl -R -x u:$SECONDARY_USER $APP_DIR
    setfacl -R -x d:u:$SECONDARY_USER $APP_DIR

elif [ $ACTION == "revoke-all" ]; then
    echo 'Revoking ALL permissions for' $SECONDARY_USER'.'
    setfacl -x u:$SECONDARY_USER $HOME
    setfacl -R -x u:$SECONDARY_USER $HOME/webapps/*
    setfacl -R -x d:u:$SECONDARY_USER $HOME/webapps/*
fi
