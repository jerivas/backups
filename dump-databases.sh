#!/bin/bash
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the ISC License as published
# in https://gitlab.com/jerivas/backups/blob/master/LICENSE

# ORIGINAL AUTHOR:  Daniel Graziotin <dgraziotin AT task3 DOT cc>
# MODIFIED BY:      Ed Rivas <e AT jerivas DOT com>
# URL:              http://ineed.coffee/1003
#                   https://gitlab.com/jerivas/backups/

# INSTRUCTIONS:     Create a text file (i.e., ~/.dblist) containing one line
#                   for each entry, with the following syntax:
#
#                   dbms_type database_name database_user
#
#                   That is, a series of space-separated values.
#                   Where dbms_type is M for MySQL and P for PostgreSQL.
#
#                   The file will be parsed when executing this script
#                   and a database backup will be created.

function ts {
	# Prints a message with a timestamp
	# Comment-out the following line to silence stdout
	echo [`date "+%Y-%m-%d %H:%M:%S"`] $1
}

BACKUP_DIR=~/backup # Folder to store all backups
DB_LIST=~/.dblist # Configuration file path

OLD_IFS="$IFS"
IFS=$'\n'

for LINE in `cat $DB_LIST`; do
	IFS=' '
	ARRAY=($LINE)
	DB_TYPE=`echo ${ARRAY[0]} | tr '[:upper:]' '[:lower:]'`
	DB_NAME=${ARRAY[1]}
	DB_USER=${ARRAY[2]}
	DB_SQL="${DB_NAME}.${DB_TYPE}sql" # Will be .psql or .msql

	if [[ ! -z "$DB_TYPE" ]] && [[ ! -z "$DB_USER" ]] && [[ ! -z "$DB_NAME" ]]; then
		ts "Starting database backup of $DB_NAME"
		mkdir -p $BACKUP_DIR/$DB_NAME
		TARGET=$BACKUP_DIR/$DB_NAME/$DB_SQL
		if [[ $DB_TYPE == "m" ]];then
			/usr/bin/mysqldump -u$DB_USER $DB_NAME > $TARGET
		elif [[ $DB_TYPE == "p" ]];then
			pg_dump -U $DB_USER -f $TARGET $DB_NAME
		else
			continue
		fi
		ts "Finished databse backup of $DB_NAME"
	fi
done

IFS="$OLD_IFS"
