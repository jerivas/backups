"""
Fabric tasks for automatically setting up backup.sh.
The required imports are not included, these tasks
are meant to be included on existing fabfiles.
"""

@task
@log_call
def setup_backups():
    """
    Setup periodic backups in the remote server.
    For Mezzanine Webf https://github.com/jerivas/mezzanine-webf
    Taken from https://gitlab.com/jerivas/backups
    """
    from fabric.api import prompt

    srv, ssn, acn = get_webf_session()
    backup_dir = "~/backup"
    media_dir = static() + "/media/"

    # Create a SFTP user
    username = prompt("SFTP username (12 chars max):")
    password = getpass("Set password for %s " % username)
    shell = "none"  # SFTP access only
    groups = []
    srv.create_user(ssn, username, shell, groups)
    srv.change_user_password(ssn, username, password)

    # Install the backup and permission scripts
    with cd("~/bin"):
        run("wget https://gitlab.com/jerivas/backups/raw/master/backup.sh")
        run("chmod +x backup.sh")
        run("wget https://gitlab.com/jerivas/backups/raw/master/permissions.sh")
        run("chmod +x permissions.sh")

    # Limit permissions of the backup user to the backup directory
    run("mkdir -p %s" % backup_dir)
    run("permissions.sh revoke-all %s" % username)
    run("permissions.sh grant %s %s" % (username, backup_dir))
    run("permissions.sh grant %s %s" % (username, media_dir))

    # Populate the configuration files
    conf = {
        "name": env.proj_name,
        "pwd": env.db_pass,
    }
    run("echo 'P {name} {name}' >> ~/.backuplist".format(**conf))
    run("chmod 600 ~/.backuplist")
    run("echo 'localhost:*:{name}:{name}:{pwd}' >> ~/.pgpass".format(**conf), show=False)
    print("Adding entry for %s to .pgpass" % env.proj_name)
    run("chmod 600 ~/.pgpass")

    # Setup cron (Sundays at 8 AM UTC)
    srv.create_cronjob(ssn, "0 8 * * 0 ~/bin/backup.sh >> %s/log.txt" % backup_dir)

    # Run once
    run("~/bin/backup.sh | tee %s/log.txt" % backup_dir)
