# Backups

Simple utilities for creating backups.

## Features

- Creates database (Postgres and MySQL) backups
- Configuration is stored in a file (automation friendly)
- Outputs timestamps for all operations

## Quickstart

```bash
# Download the script and make it executable
wget https://gitlab.com/jerivas/backups/raw/master/dump-databases.sh
chmod +x dump-databases.sh

# Add the configuration file
nano ~/.dblist
chmod 600 ~/.dblist # Prevent other users from viewing the file

# Run the backup script
./dump-databases.sh

# Check the backups in ~/backup
ls ~/backup
```

## Configuration file

The config file stores one backup job per line. Each line is a collection of
space-delimited values that define the following (in order):

- The database type ("M" for MySQL, "P" for Postgres)
- The database user
- The database name

For example:

`P mysite_db mysite_db_user`

This will backup a Postgres database called `mysite_db` connecting as
`mysite_db_user`. The resulting file will be `~/backup/mysite_db.psql`.

## FAQ

### How can I automate the backups?
Just add the script to your crontab. For example, let's run the backup every
day at midnight by adding to the crontab:

```bash
0 0 * * * /path/to/dump-databases.sh
```

If you want to log the output you can use:

```bash
# Log stdout only
0 0 * * * /path/to/dump-databases.sh >> /path/to/log.txt

# Log both stdout and stderr
0 0 * * * /path/to/dump-databases.sh >> /path/to/log.txt 2>&1
```

### Are backups rotated / old backups removed?
Nope, this creates simple backups overwriting old ones if they exist.

### Postgres and MySQL keep asking for the database password!
You won't be able to automate database backups if the script doesn't know your
database passwords. Both database vendors provide solutions out of the box:
[passwordless Postgres], [passwordless MySQL]. Note that MySQL will only let
you specify one user and one password for this.

[passwordless Postgres]: https://www.postgresql.org/docs/current/static/libpq-pgpass.html
[passwordless MySQL]: http://stackoverflow.com/a/9293090/1330003
